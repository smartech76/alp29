<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package alp29
 */

get_header();
?>

	<div class="panorama main first-banner-element" id="main" style="background-image: url(<?php echo get_template_directory_uri() . '/img/main-bg-1.jpg'?>); ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 wrapper-block">
                    <div class="wrapper-width wrapper-width-md-small">
                        <ul class="left-menu">
                            <li class="paper-block">
                                <h3 class="text"><a href="<?php echo get_page_link(16); ?>">Утепление фасада</a></h3>
                            </li>
                            <li class="paper-block">
                                <h3 class="text"><a href="<?php echo get_page_link(16); ?>">Ремонт межпанельных швов</a></h3>
                            </li>
                            <li class="paper-block">
                                 <h3 class="text"><a href="<?php echo get_page_link(16); ?>">Ремонт кровель</a></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 wrapper-block ">
                    <!-- empty block-->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 wrapper-block ">
                    
                    <div class="row">
                        <div class="wrapper-width wrapper-middle">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="icon"></div>
                                    <h1>УСЛУГИ ПРОМЫШЛЕННЫХ АЛЬПИНИСТОВ РАБОТЫ НА ВЫСОТЕ ЛЮБОЙ СЛОЖНОСТИ</h1>
                                    <div class="sub-title empty">
                                <span>
                                  <h2></h2>
                                </span>
                                    </div>
                                    <div class="text">Получите доступ к специальным ценам<br>для ТСЖ и управляющих компаний,
                                        промышленных<br> предприятий, государственных организаций<br>и коммерческих фирм:
                                    </div>
                                </div>
                                <div class="row">
                                    <form class="contactform" id="contact-form-panorama">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input id="contactform-panorama-email" type="text" name="email" placeholder="Email" class="validate[required,custom[email]] input-mobile">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input id="contactform-panorama-phone" type="text" name="phone" placeholder="Телефон" class="validate[required,custom[phone],minSize[4]] input-mobile">
                                        </div>                                  
                                        <input type='hidden' name="action" value='Получите доступ к специальным ценам для ТСЖ и управляющих  компаний, промышленных предприятий, государственных организаций и коммерческих фирм'/>
                                        <input type='hidden' name="slide" value=""/>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <input type="submit" class="button red-button input-mobile" value="ПОЛУЧИТЬ ДОСТУП">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="row">
                                                <label class="white-link"><input type="checkbox" class="" id="access-personal-information">Я согласен на обработку персональных данных</label>
                                                <a href="" class="white-link">Текст соглашения</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="contact-form-7-wrapper" id="contact-form-7-panorama">
                                        <?php echo do_shortcode('[contact-form-7 id="12" title="ContactFormPanorama"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page404">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <h2 class="size-big">404</h2>
                    </div>
                    <div class="row">
                        <h2 class="size-big">Такой страницы нет!</h2>
                    </div>
                    <div class="row">
                        <h2>Вернуться на главную <a href="<?php echo get_home_url(); ?>">http://alp29.ru/</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="back-build">
        </div>
    </div>

    <div class="question">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper">
                        <div class="section-title">У Вас остались вопросы?</div>
                        <h2>Задайте их нашему специалисту.</h2>
                        <div class="ask-button button red-button">ЗАДАТЬ ВОПРОС</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
