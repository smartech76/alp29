$( document ).ready(function() {
    
    //Shift the background to the right if the main block is high
    var $main = $("#main");
    var mainHeight = $main.height();
    var maxHeight = 650;
    if (mainHeight > maxHeight) {
        $main.addClass("main-background-position");
    }
    
});