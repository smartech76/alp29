<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package alp29
 */

get_header();
?>

<div class="panorama main first-banner-element" id="main" style="background-image: url(<?php echo get_template_directory_uri() . '/img/main-bg-1.jpg'?>); ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 wrapper-block">
                    <div class="wrapper-width wrapper-width-md-small">
                        <ul class="left-menu">
                            <li class="paper-block">
                                <h3 class="text"><a href="<?php echo get_page_link(16); ?>">Утепление фасада</a></h3>
                            </li>
                            <li class="paper-block">
                                <h3 class="text"><a href="<?php echo get_page_link(16); ?>">Ремонт межпанельных швов</a></h3>
                            </li>
                            <li class="paper-block">
                                 <h3 class="text"><a href="<?php echo get_page_link(16); ?>">Ремонт кровель</a></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 wrapper-block ">
                    <!-- empty block-->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 wrapper-block ">
                    
                    <div class="row">
                        <div class="wrapper-width wrapper-middle wrapper-panorama-form">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="icon"></div>
                                    <h1>УСЛУГИ ПРОМЫШЛЕННЫХ АЛЬПИНИСТОВ РАБОТЫ НА ВЫСОТЕ ЛЮБОЙ СЛОЖНОСТИ</h1>
                                    <div class="sub-title empty">
                                <span>
                                  <h2></h2>
                                </span>
                                    </div>
                                    <div class="text text-sale">Получите доступ к специальным ценам для ТСЖ и управляющих компаний,
                                        промышленных предприятий, государственных организаций и коммерческих фирм:
                                    </div>
                                </div>
                                <div class="row">
                                    <form class="contactform" id="contact-form-panorama">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input id="contactform-panorama-email" type="text" name="email" placeholder="Email" class="validate[required,custom[email]] input-mobile">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input id="contactform-panorama-phone" type="text" name="phone" placeholder="Телефон" class="validate[required,custom[phone],minSize[4]] input-mobile">
                                        </div>                                  
                                        <input type='hidden' name="action" value='Получите доступ к специальным ценам для ТСЖ и управляющих  компаний, промышленных предприятий, государственных организаций и коммерческих фирм'/>
                                        <input type='hidden' id="contactform-panorama-subject" name="contactform-panorama-subject" value='<?php get_mail_subject(); ?>'/>
                                        <input type='hidden' name="slide" value=""/>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <input type="submit" class="button red-button input-mobile" value="ПОЛУЧИТЬ ДОСТУП">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="row">
                                                <input type="checkbox" checked="checked" name="panorama-personal-information" class="validate[required]" id="panorama-personal-information">
                                                <label class="white-link label-margin">Я согласен на обработку персональных данных</label>
                                                <a href="<?php echo get_page_link(14); ?>" class="white-link">Текст соглашения</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="contact-form-7-wrapper" id="contact-form-7-panorama">
                                        <?php echo do_shortcode('[contact-form-7 id="12" title="ContactFormPanorama"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="wrapper-width wrapper-width-md">
                            <div class="paper-block">
                                <div class="sale-block">
                                    <span class="glyphicon glyphicon glyphicon-gift"></span>
                                    <span class="text">Акция</span>
                                </div>
                                <div class="sub-title empty bottom-margin-small">
                                <span>
                                  <h2></h2>
                                </span>
                                </div>
                                <div class="text sale-text">Скоро Новый улучшенный интерфейс сайта!</div>
                                <div class="timer white-text hidden"><?php echo do_shortcode('[wpcdt-countdown id="5"]'); ?></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="advantages">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 advantage-block">
                    <div class="advantage">
                        <div class="image old"></div>
                        <div class="text">12 лет<br> на рынке</div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 advantage-block">
                    <div class="advantage">
                        <div class="image success_projects"></div>
                        <div class="text">Более 300 <br> выполненных<br> проектов</div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 advantage-block">
                    <div class="advantage">
                        <div class="image guaranty"></div>
                        <div class="text">5 лет<br> гарантия на все виды<br> услуг</div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 advantage-block">
                    <div class="advantage">
                        <div class="image speed"></div>
                        <div class="text">Готовы приступить<br>к работе в день<br>обращения</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="spheres" id="spheres">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">ВЫБЕРИТЕ ВАШУ СФЕРУ</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <div class="tab active" data-target="hoa">
                            <div class="name">ТСЖ и управляющие компании</div>
                            <div class="objects">Более 300 объектов</div>
                        </div>
                        <div class="tab" data-target="industrial">
                            <div class="name">Промышленные предприятия</div>
                            <div class="objects">3 крупных объекта</div>
                        </div>
                        <div class="tab" data-target="state">
                            <div class="name">Государственные организации</div>
                            <div class="objects">Более 15 объектов</div>
                        </div>
                        <div class="tab" data-target="commercial">
                            <div class="name">Коммерческие организации</div>
                            <div class="objects">Более 70 объектов</div> 
                        </div>
                    </div>

                    <div class="sphere hoa">
                        <div class="title">УСЛУГИ ДЛЯ ТСЖ И УПРАВЛЯЮЩИХ КОМПАНИЙ:</div>
                        <div class="divider"></div>
                        <img src="<?php  echo get_template_directory_uri() . '/img/spheres/hoa.jpg'?>" alt="">
                        <ul>
                            <li>Ремонт и герметизация межпанельных швов</li>
                            <li>Ремонт любых видов кровель</li>
                            <li>Очистка кровли от снега и наледи</li>
                            <li>Ремонт водосточных систем<br>и ливневой канализации</li>
                        </ul>
                        <ul>
                            <li>Уборка аварийных деревьев</li>
                            <li>Очистка и восстановление вентиляции</li>
                            <li>Ремонт фасадов</li>
                        </ul>
                    </div>
                    <div class="sphere hidden-r industrial">
                        <div class="title">УСЛУГИ ДЛЯ ПРОМЫШЛЕННЫХ ПРЕДПРИЯТИЙ:</div>
                        <div class="divider"></div>
                        <img src="<?php  echo get_template_directory_uri() . '/img/spheres/industrial.jpg'?>" alt="">
                        <ul>
                            <li>Окраска металлических и железобетонных конструкций</li>
                            <li>Огнезащита конструкций</li>
                            <li>Монтаж металлоконструкций<br>(в том числе в стесненных условиях)</li>
                            <li>Демонтаж зданий и сооружений,<br>дымовых труб котельных любой сложности и высоты</li>
                        </ul>
                        <ul>
                            <li>Ремонт фасадов</li>
                            <li>Ремонт и герметизация швов</li>
                            <li>Ремонт кровель</li>
                            <li>Локальный демонтаж панелей<br>и стеновых панелей зданий</li>
                        </ul>
                    </div>
                    <div class="sphere hidden-r state">
                        <div class="title">УСЛУГИ ДЛЯ ГОСУДАРСТВЕННЫХ ОРГАНИЗАЦИЙ:</div>
                        <div class="divider"></div>
                        <img src="<?php  echo get_template_directory_uri() . '/img/spheres/state.jpg'?>" alt="">
                        <ul>
                            <li>Ремонт и герметизация межпанельных швов</li>
                            <li>Ремонт любых видов кровель</li>
                            <li>Очистка кровли от снега и наледи</li>
                            <li>Ремонт водосточных систем<br>и ливневой канализации</li>
                        </ul>
                        <ul>
                            <li>Очистка и восстановление вентиляции</li>
                            <li>Ремонт фасадов</li>
                        </ul>
                    </div>
                    <div class="sphere hidden-r commercial">
                        <div class="title">УСЛУГИ ДЛЯ КОММЕРЧЕСКИХ ОРГАНИЗАЦИЙ:</div>
                        <div class="divider"></div>
                        <img src="<?php  echo get_template_directory_uri() . '/img/spheres/commercial.jpg'?>" alt="">
                        <ul>
                            <li>Ремонт и герметизация межпанельных швов</li>
                            <li>Ремонт любых видов кровель</li>
                            <li>Очистка кровли от снега и наледи</li>
                            <li>Ремонт водосточных систем<br>и ливневой канализации</li>
                        </ul>
                        <ul>
                            <li>Очистка и восстановление вентиляции</li>
                            <li>Ремонт фасадов</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="access">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="section-title">СПЕЦИАЛЬНЫЕ ЦЕНЫ</div>
                        <div class="text hoa">ПОЛУЧИТЕ ДОСТУП К СПЕЦИАЛЬНЫМ ЦЕНАМ ДЛЯ ТСЖ И УПРАВЛЯЮЩИХ КОМПАНИЙ</div>
                        <div class="text custom-hidden industrial">ПОЛУЧИТЕ ДОСТУП К СПЕЦИАЛЬНЫМ ЦЕНАМ ДЛЯ ПРОМЫШЛЕННЫХ
                            ПРЕДПРИЯТИЙ
                        </div>
                        <div class="text custom-hidden state">ПОЛУЧИТЕ ДОСТУП К СПЕЦИАЛЬНЫМ ЦЕНАМ ДЛЯ ГОСУДАРСТВЕННЫХ
                            ОРГАНИЗАЦИЙ
                        </div>
                        <div class="text custom-hidden commercial">ПОЛУЧИТЕ ДОСТУП К СПЕЦИАЛЬНЫМ ЦЕНАМ ДЛЯ КОММЕРЧЕСКИХ
                            ОРГАНИЗАЦИЙ
                        </div>
                    </div>
                    <div class="row">
                        <form class="contactform" id="contact-form-access" data-ga="ga('send', 'event', 'request', 'send');">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="access-input-container">
                                    <div class="access-input-block">
                                        <input id="contactform-access-email" type="text" name="email" placeholder="Email" class="validate[required,custom[email]] input-mobile">
                                    </div>
                                    <div class="access-input-block">
                                        <input id="contactform-access-phone" type="text" name="phone" placeholder="Телефон" class="validate[required,custom[phone],minSize[4]] input-mobile">
                                    </div>
                                    <div class="access-input-block">
                                        <input type="submit" id="contactform-access-submit" class="button red-button input-mobile input-submit" value="ПОЛУЧИТЬ ДОСТУП">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="row">
                                    <input type="checkbox" checked="checked" name="access-personal-information" class="validate[required]" id="access-personal-information">
                                    <label class="white-link label-margin">Я согласен на обработку персональных данных</label>
                                    <a href="<?php echo get_page_link(14); ?>" class="white-link">Текст соглашения</a>
                                </div>
                            </div>
                        </form>
                        <div class="contact-form-7-wrapper" id="contact-form-7-access">
                            <?php echo do_shortcode('[contact-form-7 id="10" title="ContactFormAccess"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="projects">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="section-title">ПОСЛЕДНИЕ ВЫПОЛНЕННЫЕ ПРОЕКТЫ</div>
                    <div class="slider-container">
                        <div class="slider hoa">
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/hoa/1.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ООО «Октябрьский»</h2>
                                            <div class="text">Для ООО «Октябрьский» был произведен полный комплекс
                                                работ:
                                            </div>
                                            <ul>
                                                <li>Ремонт кровель</li>
                                                <li>Ремонт фасадов</li>
                                                <li>Ремонт стенных межпанельных швов</li>
                                                <li>Ремонт ливневой канализации</li>
                                                <li>Очистка снега и наледи с кровель зданий</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4  projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/hoa/2.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ТСЖ «Осипенко 5»</h2>
                                            <div class="text">Для ТСЖ «Осипенко 5» были проведены следующие работы:
                                            </div>
                                            <ul>
                                                <li>Ремонт кровли</li>
                                                <li>Ремонт межпанельных швов</li>
                                                <li>Очистка ливневой канализации</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/hoa/3.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ЖСК «Ломоносовский 5»</h2>
                                            <div class="text">Для ЖСК «Ломоносовский 5» были произведены следующие
                                                работы:
                                            </div>
                                            <ul>
                                                <li>Ремонт межпанельных швов</li>
                                                <li>Ремонт кровли</li>
                                                <li>Очистка ливневой канализации</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider industrial">
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/industrial/1.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ЗАО «Архангельский фанерный завод»</h2>
                                            <div class="text">Для ЗАО «Архангельский фанерный завод» был произведен
                                                сложный комплекс работ:
                                            </div>
                                            <ul>
                                                <li>Капитальный ремонт кровли из металлочерепицы</li>
                                                <li>Монтажные работы по усилению металлоконструкций в сушильном цехе
                                                </li>
                                                <li>Демонтаж вентиляционных куполов сушильного цеха</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/industrial/2.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>Архангельский ЦБК</h2>
                                            <div class="text">Для Архангельского ЦБК был проведен комплекс сложных
                                                работ:
                                            </div>
                                            <ul>
                                                <li>Демонтаж стеновых железобетонных панелей</li>
                                                <li>Усиление несущих колонн</li>
                                                <li>Монтаж сэндвич панелей</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/industrial/3.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ООО «СилБет»</h2>
                                            <div class="text">Для ООО «СилБет» был произведен демонтаж оголовка
                                                кирпичной дымовой трубы котельной
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/industrial/4.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>МЛСП «Приразломная»</h2>
                                            <div class="text">для МЛСП «Приразломная» был проведен комплекс работ в
                                                условиях ограниченного доступа
                                            </div>
                                            <ul>
                                                <li>Сборка, монтаж и демонтаж узлов оборудования бурового комплекса</li>
                                                <li>Очистка и смазка оборудования бурового комплекса</li>
                                                <li>Проведение огневых работ на высоте</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider state">
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/state/1.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>Северный государственный медицинский университет</h2>
                                            <div class="text">Для ГБОУ ВПО СГМУ регулярно проводятся следующие работы по
                                                обслуживанию кровли:
                                            </div>
                                            <ul>
                                                <li>Ремонт кровли</li>
                                                <li>Очистка кровли от снега и наледи</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/state/2.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>Государственное автономное учреждение Архангельской области «Центр
                                                изучения общественного мнения»</h2>
                                            <div class="text">Для центра изучения общественного мнения были проведены
                                                работы по очистке кровли от снега
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/state/3.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>Архангельский краеведческий музей</h2>
                                            <div class="text">Для Архангельского краеведческого музея на регулярной
                                                основе производится обслуживание кровли
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="slider commercial">
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/commercial/1.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>Сеть магазинов «Магнит» </h2>
                                            <div class="text">Для ЗАО «Тандер» проводится регулярное обслуживание
                                                кровель
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/commercial/2.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ОАО «Молоко»</h2>
                                            <div class="text">Для ОАО «Молоко» был произведен ремонт межпанельных швов
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="row-fluid">
                                    <div class="col-xs-12 col-sm-12 col-md-4 projects-image">
                                        <img class="img-responsive" src="<?php  echo get_template_directory_uri() . '/img/projects/commercial/3.jpg'?>" alt="">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8">
                                        <div class="info">
                                            <h2>ОАО «Северный дом»</h2>
                                            <div class="text">Для ОАО «Северный дом» были произведены сложные работы на
                                                высоте:
                                            </div>
                                            <ul>
                                                <li>Монтажные работы</li>
                                                <li>Монтаж кронштейнов для систем вентиляции</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clients">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="section-title">С НАМИ УЖЕ СОТРУДНИЧАЮТ</div>
                    </div>

                    <div class="row">
                        <div class="slider hoa">
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/hoa/dmitryi.jpg'?>" alt="">
                                    <div class="name">Дмитрий Сергеевич Орловский</div>
                                    <div class="post">Генеральный директор ООО «Октябрьский»</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/hoa/roman.jpg'?>" alt="">
                                    <div class="name">Роман Александрович Лысков</div>
                                    <div class="post">Директор УК 11 «Наш дом – Архангельск»</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/hoa/olga.jpg'?>" alt="">
                                    <div class="name">Ольга Михайловна</div>
                                    <div class="post">Заместитель директора управляющей компании «Деком»</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="slider industrial hidden">
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/industrial/pavel_b.jpg'?>" alt="">
                                    <div class="name">Павел Клавдиевич Бурчаловский</div>
                                    <div class="post">Генеральный директор ЗАО «Архангельский фанерный завод»</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/industrial/andrei.jpg'?>" alt="">
                                    <div class="name">Андрей Владимирович Артемьев</div>
                                    <div class="post">Главный строитель АЦБК</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/industrial/pavel_k.jpg'?>" alt="">
                                    <div class="name">Павел Евгеньевич Кубышкин</div>
                                    <div class="post">Генеральный директор ООО «СилБет»</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="slider state hidden">
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/state/vladimir.jpg'?>" alt="">
                                    <div class="name">Владимир Николаевич Козицин</div>
                                    <div class="post">Начальник ремонтно-строительного отдела СГМУ</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/state/elena.jpg'?>" alt="">
                                    <div class="name">Елена Николаевна Ерофеева</div>
                                    <div class="post">Архангельский краеведческий музей</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/state/sergei.jpg'?>" alt="">
                                    <div class="name">Сергей Васильевич Стасюк</div>
                                    <div class="post">Государственное автономное учреждение Архангельской области «Центр
                                        изучения общественного мнения»
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="slider commercial hidden">
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/commercial/aleksandr.jpg'?>" alt="">
                                    <div class="name">Александр Николаевич Черняков</div>
                                    <div class="post">Руководитель по строительству ОАО «Северный дом»</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/commercial/sergei_a.jpg'?>" alt="">
                                    <div class="name">Сергей Александрович</div>
                                    <div class="post">ОАО «Молоко»</div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="slide">
                                    <img src="<?php  echo get_template_directory_uri() . '/img/clients/commercial/sergei_e.jpg'?>" alt="">
                                    <div class="name">Сергей Евгеньевич Усов</div>
                                    <div class="post">Главный инженер, сеть магазинов «Магнит»</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="why-we">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top"></div>
                    <div class="section-title">ПОЧЕМУ ВЫБИРАЮТ ИМЕННО НАС:</div>
                    <div class="reasons hoa">
                        <div class="reason">
                            <div class="image">
                                <div class="five"></div>
                            </div>
                            <div class="text">Гарантия<br>5 лет</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="purse"></div>
                            </div>
                            <div class="text">Возможность<br>рассрочки платежа</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="clock"></div>
                            </div>
                            <div class="text">Готовы приступить<br>к работе в день<br>обращения</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="hand"></div>
                            </div>
                            <div class="text">Возможность оплаты<br>по факту выполнения</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="check"></div>
                            </div>
                            <div class="text">Гарантия<br>качества</div>
                        </div>
                        <div class="divider"></div>
                        <div class="additional">Весь спектр услуг под ключ, в том числе ремонт подъездов, тепловых<br>узлов,
                            электрических, сантехнических и канализационных систем,<br>установка дверей, почтовых ящиков
                            и окон на лестничных клетках.
                        </div>
                        <div class="additional">Возможность заключения договоров<br>на обслуживание кровель<br>или
                            зданий полностью.
                        </div>
                    </div>
                    <div class="reasons industrial hidden">
                        <div class="reason">
                            <div class="image">
                                <div class="five"></div>
                            </div>
                            <div class="text">Гарантия<br>5 лет</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="cro"></div>
                            </div>
                            <div class="text">Наличие<br>лицензии СРО</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="work"></div>
                            </div>
                            <div class="text">Выполнение<br>комплекса работ<br>под ключ</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="specialists"></div>
                            </div>
                            <div class="text">Аттестованные специалисты<br>с удостоверениями<br> промышленных
                                альпинистов
                            </div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="check"></div>
                            </div>
                            <div class="text">Гарантия<br>качества</div>
                        </div>
                        <div class="divider"></div>
                        <div class="reason wide">
                            <div class="image">
                                <div class="certificate"></div>
                            </div>
                            <div class="text">Сертификаты на все<br>материалы</div>
                        </div>
                        <div class="reason wide">
                            <div class="image">
                                <div class="ppr"></div>
                            </div>
                            <div class="text">Составление ППР<br><span>(проекта производства работ)</span></div>
                        </div>
                        <div class="reason wide">
                            <div class="image">
                                <div class="calculates"></div>
                            </div>
                            <div class="text">Предоставление сметных<br>расчетов без дополнительных<br>наценок в
                                процессе и после<br>выполнения работ
                            </div>
                        </div>
                        <div class="reason wide">
                            <div class="image">
                                <div class="instruments"></div>
                            </div>
                            <div class="text">Своё современное<br>оборудование,<br>снаряжение и техника</div>
                        </div>
                    </div>
                    <div class="reasons state hidden">
                        <div class="reason">
                            <div class="image">
                                <div class="five"></div>
                            </div>
                            <div class="text">Гарантия<br>5 лет</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="tender"></div>
                            </div>
                            <div class="text">Участие в тендерных<br>закупках</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="all-in-one"></div>
                            </div>
                            <div class="text">Весь спектр услуг<br>под ключ</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="modern-tools"></div>
                            </div>
                            <div class="text">Своё современное<br>оборудование,<br>снаряжение и техника</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="check"></div>
                            </div>
                            <div class="text">Гарантия<br>качества</div>
                        </div>
                        <div class="divider"></div>
                        <div class="additional">Заключение договоров на обслуживание кровель<br>и полное обслуживание
                            зданий
                        </div>
                        <div class="additional">Возможность выполнения работ<br>в сжатые сроки</div>
                    </div>
                    <div class="reasons commercial hidden">
                        <div class="reason">
                            <div class="image">
                                <div class="five"></div>
                            </div>
                            <div class="text">Гарантия<br>5 лет</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="all-in-one"></div>
                            </div>
                            <div class="text">Весь спектр услуг<br>под ключ</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="modern-tools"></div>
                            </div>
                            <div class="text">Своё современное<br>оборудование,<br>снаряжение и техника</div>
                        </div>
                        <div class="reason">
                            <div class="image">
                                <div class="check"></div>
                            </div>
                            <div class="text">Гарантия<br>качества</div>
                        </div>
                        <div class="divider"></div>
                        <div class="additional">Возможность выполнения работ в сжатые сроки:<br>готовы приступить к
                            работе в день обращения
                        </div>
                        <div class="additional">Заключение договоров на обслуживание кровель<br>и полное обслуживание
                            зданий
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="feedbacks" id="feedbacks">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">ОТЗЫВЫ О НАШЕЙ РАБОТЕ</div>

                    <div class="slider-container">
                        <div class="slider">
                            <div class="slide" data-big="<?php echo get_template_directory_uri() . '/img/20170228-IP-Mokretsov-Blagodarstvennoe-pismo.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/smal1.jpg'?>" alt="">
                                <div class="text">Газпром</div>
                            </div>
                            <div class="slide" data-big="<?php  echo get_template_directory_uri() . '/img/feedbacks/big/arh-post.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/feedbacks/arh-post.jpg'?>" alt="">
                                <div class="text">Архангельский почтамт</div>
                            </div>
                            <div class="slide" data-big="<?php  echo get_template_directory_uri() . '/img/feedbacks/big/aokb.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/feedbacks/aokb.jpg'?>" alt="">
                                <div class="text">АОКБ</div>
                            </div>
                            <div class="slide" data-big="<?php  echo get_template_directory_uri() . '/img/feedbacks/big/zao-afz.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/feedbacks/zao-afz.jpg'?>" alt="">
                                <div class="text">ЗАО АФЗ</div>
                            </div>
                            <div class="slide" data-big="<?php  echo get_template_directory_uri() . '/img/feedbacks/big/uk11.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/feedbacks/uk11.jpg'?>" alt="">
                                <div class="text">УК11 Наш дом-Архангельск</div>
                            </div>
                            <div class="slide" data-big="<?php  echo get_template_directory_uri() . '/img/feedbacks/big/sgmu.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/feedbacks/sgmu.jpg'?>" alt="">
                                <div class="text">СГМУ</div>
                            </div>
                            <div class="slide" data-big="<?php  echo get_template_directory_uri() . '/img/feedbacks/big/kotl-tip.jpg'?>">
                                <img src="<?php  echo get_template_directory_uri() . '/img/feedbacks/kotl-tip.jpg'?>" alt="">
                                <div class="text">Котласская типография</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="question">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper">
                        <div class="section-title">У Вас остались вопросы?</div>
                        <h2>Задайте их нашему специалисту.</h2>
                        <div class="ask-button button red-button">ЗАДАТЬ ВОПРОС</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map-wrapper" id="map2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 no-padding">
                    <div class="map" id="map">
                        <div class="top">
                            <div class="section-title">НАШИ КООРДИНАТЫ:</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
<?php
get_footer();
